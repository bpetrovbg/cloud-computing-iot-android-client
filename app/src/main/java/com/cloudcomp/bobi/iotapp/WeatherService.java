package com.cloudcomp.bobi.iotapp;

import android.util.Log;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class WeatherService {
    private static WeatherService instance = null;
    private WeatherApiService weatherApiService;
    public static final String WEATHER_API_KEY = "71bd5477fb9edef5235c4e960aff94f1";

    public static WeatherService getInstance() {
        if(instance == null) {
            instance = new WeatherService();
        }

        return instance;
    }

    private WeatherService() { weatherApiService = Api.getInstance().getWeatherService(); }

    public void getWeatherInfo(final Double lat, final Double lon, final IApiRequestCallback<WeatherData> callback) {
        weatherApiService.getWeather(lat, lon, WEATHER_API_KEY).subscribe(new SingleObserver<WeatherData>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(WeatherData weatherData) {
                callback.onSuccess(weatherData);
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }
}
