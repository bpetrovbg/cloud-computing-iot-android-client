package com.cloudcomp.bobi.iotapp;

import com.google.gson.annotations.SerializedName;

public class Door {
    @SerializedName("id")
    private Integer id;
    @SerializedName("description")
    private String description;
    @SerializedName("opened")
    private Boolean isOpened;

    public Door(Integer id, String description, Boolean isOpened) {
        this.id = id;
        this.description = description;
        this.isOpened = isOpened;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getOpened() {
        return isOpened;
    }
}