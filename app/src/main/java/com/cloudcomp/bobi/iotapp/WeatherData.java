package com.cloudcomp.bobi.iotapp;

import com.google.gson.annotations.SerializedName;

public class WeatherData {
    @SerializedName("weather")
    private WeatherPOJO[] weather;
    @SerializedName("main")
    private MainPOJO main;
    @SerializedName("name")
    private String name;
    @SerializedName("sys")
    private SysPOJO sys;

    public WeatherData() {
    }

    public WeatherData(WeatherPOJO[] weather, MainPOJO main, String name, SysPOJO sys) {
        this.weather = weather;
        this.main = main;
        this.name = name;
        this.sys = sys;
    }

    public WeatherPOJO[] getWeather() {
        return weather;
    }

    public void setWeather(WeatherPOJO[] weather) {
        this.weather = weather;
    }

    public MainPOJO getMain() {
        return main;
    }

    public void setMain(MainPOJO main) {
        this.main = main;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SysPOJO getSys() {
        return sys;
    }

    public void setSys(SysPOJO sys) {
        this.sys = sys;
    }
}
