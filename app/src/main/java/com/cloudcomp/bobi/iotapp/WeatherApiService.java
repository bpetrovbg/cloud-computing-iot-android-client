package com.cloudcomp.bobi.iotapp;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherApiService {
    @GET("weather")
    Single<WeatherData> getWeather(@Query("lat") Double lat, @Query("lon") Double lon, @Query("appid") String APIKEY);
}
