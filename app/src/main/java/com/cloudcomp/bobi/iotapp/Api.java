package com.cloudcomp.bobi.iotapp;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {
    private static Api instance = null;

    private static final String BASE_URL = "http://192.168.100.57:8080/cc/";

    private DoorsApiService doorsService;
    private WeatherApiService weatherService;

    public static Api getInstance(){
        if (instance == null){
            instance = new Api();
        }

        return instance;
    }

    private Api(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();

        doorsService = retrofit.create(DoorsApiService.class);

        Retrofit retrofitWeather = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
        weatherService = retrofitWeather.create(WeatherApiService.class);
    }

    public DoorsApiService getDoorsService() { return doorsService; }

    public WeatherApiService getWeatherService() { return weatherService; }
}