package com.cloudcomp.bobi.iotapp;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class DoorsService {
    private static DoorsService instance = null;
    private DoorsApiService doorsApiService;
    private List<Door> doorList = new ArrayList<>();

    public static DoorsService getInstance() {
        if (instance == null) {
            instance = new DoorsService();
        }

        return instance;
    }

    private DoorsService() {
        doorsApiService = Api.getInstance().getDoorsService();
    }

    public void pullAllDoors(final IApiRequestCallback<List<Door>> callback) {
        doorsApiService.getDoors().subscribe(new SingleObserver<List<Door>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(List<Door> doors) {
                doorList = doors;
                callback.onSuccess(doors);
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }

    public void changeDoorState(final Integer doorId, final IApiRequestCallback<Door> callback) {
        doorsApiService.changeDoorState(doorId).subscribe(new SingleObserver<Door>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Door door) {
                callback.onSuccess(door);
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }

    public List<Door> getDoors() {
        return doorList;
    }
}
