package com.cloudcomp.bobi.iotapp;

public interface IApiRequestCallback<T> {
    void onSuccess(T t);
    void onError(Throwable e);
}
