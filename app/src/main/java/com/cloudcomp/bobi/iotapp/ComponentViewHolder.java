package com.cloudcomp.bobi.iotapp;

import android.app.Activity;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class ComponentViewHolder extends RecyclerView.ViewHolder {
    private TextView componentLabel;
    public Switch componentSwitch;
    public Door door;

    public ComponentViewHolder(final View itemView) {
        super(itemView);
        componentLabel = itemView.findViewById(R.id.component_label);
        componentSwitch = itemView.findViewById(R.id.component_label_switch);
    }

    public void bindData(final Door door) {
        this.door = door;
        componentLabel.setText(door.getDescription());
        if (door.getOpened() != null) {
            componentSwitch.setChecked(door.getOpened());
        }
        componentSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                DoorsService.getInstance().changeDoorState(door.getId(), new IApiRequestCallback<Door>() {
                    @Override
                    public void onSuccess(final Door door) {
                        ((Activity) view.getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                componentSwitch.setChecked(door.getOpened());
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {
                        componentSwitch.setChecked(false);
                    }
                });
            }
        });
    }
}
