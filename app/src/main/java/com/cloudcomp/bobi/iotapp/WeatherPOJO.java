package com.cloudcomp.bobi.iotapp;

import com.google.gson.annotations.SerializedName;

class WeatherPOJO {
    @SerializedName("description")
    private String description;

    public WeatherPOJO() {
    }

    public WeatherPOJO(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "WeatherPOJO{" +
                "description='" + description + '\'' +
                '}';
    }
}
