package com.cloudcomp.bobi.iotapp;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DoorsApiService {
    @GET("doors")
    Single<List<Door>> getDoors();

    @POST("doors/{doorId}")
    Single<Door> changeDoorState(@Path("doorId") Integer doorId);
}
