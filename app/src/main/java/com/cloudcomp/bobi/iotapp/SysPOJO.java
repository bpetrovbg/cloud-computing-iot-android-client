package com.cloudcomp.bobi.iotapp;

import com.google.gson.annotations.SerializedName;

public class SysPOJO {
    @SerializedName("sunrise")
    private Long sunrise;
    @SerializedName("sunset")
    private Long sunset;

    public SysPOJO() {
    }

    public SysPOJO(Long sunrise, Long sunset) {
        this.sunrise = sunrise;
        this.sunset = sunset;
    }

    public Long getSunrise() {
        return sunrise;
    }

    public void setSunrise(Long sunrise) {
        this.sunrise = sunrise;
    }

    public Long getSunset() {
        return sunset;
    }

    public void setSunset(Long sunset) {
        this.sunset = sunset;
    }

    @Override
    public String toString() {
        return "SysPOJO{" +
                "sunrise=" + sunrise +
                ", sunset=" + sunset +
                '}';
    }
}
