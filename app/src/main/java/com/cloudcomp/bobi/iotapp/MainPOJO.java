package com.cloudcomp.bobi.iotapp;

import com.google.gson.annotations.SerializedName;

class MainPOJO {
    @SerializedName("temp")
    private Double temp;
    @SerializedName("feels_like")
    private Double feels_like;
    @SerializedName("humidity")
    private Integer humidity;

    public MainPOJO() {
    }

    public MainPOJO(Double temp, Double feels_like, Integer humidity) {
        this.temp = temp;
        this.feels_like = feels_like;
        this.humidity = humidity;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Double getFeels_like() {
        return feels_like;
    }

    public void setFeels_like(Double feels_like) {
        this.feels_like = feels_like;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "MainPOJO{" +
                "temp=" + temp +
                ", feels_like=" + feels_like +
                ", humidity=" + humidity +
                '}';
    }
}
