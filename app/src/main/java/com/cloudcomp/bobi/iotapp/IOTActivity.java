package com.cloudcomp.bobi.iotapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class IOTActivity extends AppCompatActivity {
    private RecyclerView doors;
    private ArrayAdapter<Door> doorArrayAdapter;
    private List<Door> doorList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iot);

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
            return;
        }

        locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                WeatherService.getInstance().getWeatherInfo(location.getLatitude(), location.getLongitude(), new IApiRequestCallback<WeatherData>() {
                    @Override
                    public void onSuccess(final WeatherData weatherData) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView weatherInfo = findViewById(R.id.weather_info);

                                SimpleDateFormat df = new SimpleDateFormat("hh:mm");
                                String sunrise = df.format(new Date(weatherData.getSys().getSunrise()));
                                String sunset = df.format(new Date(weatherData.getSys().getSunset()));

                                weatherInfo.setText(" The weather in: " + weatherData.getName()
                                        + " is: " + weatherData.getWeather()[0].getDescription() +
                                        "\n The temperature is: " + ((int) Math.round(weatherData.getMain().getTemp() - 273.15)) + "C"
                                        + " , but feels like: " + ((int) Math.round(weatherData.getMain().getFeels_like() - 273.15)) + "C"
                                        + " \n The humidity is: " + weatherData.getMain().getHumidity()
                                        + "\n Sunrise: " + sunrise + " AM"
                                        + "\n Sunset: " + sunset + " PM");
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        }, null);

        DoorsService.getInstance().pullAllDoors(new IApiRequestCallback<List<Door>>() {
            @Override
            public void onSuccess(final List<Door> doors) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        processData(doors);
                    }
                });
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }

    private void processData(final List<Door> loadedDoors) {
        this.doors = findViewById(R.id.components_list);
        doors.setHasFixedSize(true);
        doors.setLayoutManager(new LinearLayoutManager(this));
        doors.setAdapter(new RecyclerView.Adapter() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                final View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
                return new ComponentViewHolder(view);
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ((ComponentViewHolder) holder).bindData(loadedDoors.get(position));
            }

            @Override
            public int getItemCount() {
                return loadedDoors.size();
            }

            @Override
            public int getItemViewType(int position) {
                return R.layout.component_item;
            }
        });
    }
}
